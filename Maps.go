package main

import (
	"golang.org/x/tour/wc";
	"strings"
)

func WordCount(s string) map[string]int {
	mymap := make(map[string]int)
	for _, i := range strings.Fields(s) {
		mymap[i] = mymap[i] + 1
	}
	return mymap
}

func main() {
	wc.Test(WordCount)
}
