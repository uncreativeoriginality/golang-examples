package main

import "fmt"

type mytype int

type Inter interface {
	do_thing() int
}

type NewInter interface {
	do_another() int
}


func (x mytype) do_thing() int {
	return int(x + 1)
}

func (x mytype) do_another() int {
	return int(x + 10)
}


func main() {
	var x mytype = 8
	fmt.Println(x.do_thing())
	fmt.Println(x.do_another())
}
