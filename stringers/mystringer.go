package main

import "fmt"

type IPAddr [4]byte

// TODO: Add a "String() string" method to IPAddr.
type Methods interface {
	String() string
}

func (x IPAddr) String() string {
	/*
	var s = []string
	for _, i = range x {
		s.append(
	*/
	return fmt.Sprintf("%d.%d.%d.%d", x[0], x[1], x[2], x[3])
}
func main() {
	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}
	for name, ip := range hosts {
		fmt.Printf("%v: %v\n", name, ip)
	}
}

